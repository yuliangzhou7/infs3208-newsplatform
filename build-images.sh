#!/bin/bash/

# Build images tagged with location of GCR/project-name/image-name
docker build -t gcr.io/infs3208-newsplatform/nginx:latest ./nginx
docker build -t gcr.io/infs3208-newsplatform/client:v2.1 ./client
docker build -t gcr.io/infs3208-newsplatform/server:v2.1 ./server
docker build -t gcr.io/infs3208-newsplatform/pyspark:latest ./spark

# Push build images to GCR
docker push gcr.io/infs3208-newsplatform/nginx:latest
docker push gcr.io/infs3208-newsplatform/client:v2.1
docker push gcr.io/infs3208-newsplatform/server:v2.1
docker push gcr.io/infs3208-newsplatform/pyspark:latest
