credentials        = "./credentials/terraform-gke-keyfile.json"
project_id         = "infs3208-newsplatform"
region             = "us-central1"
zone               = "us-central1-a"
machine_types       = { "master" = "n1-standard-2", "slave" = "g1-small" }
disk_image         = "cos-cloud/cos-stable"
disk_size_gb       = 30
cidrs              = [ "10.0.0.0/16", "10.1.0.0/16" ]
