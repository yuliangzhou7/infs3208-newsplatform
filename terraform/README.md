## Installation

Install [Terraform CLI](https://www.terraform.io/downloads.html)

### Setup service account credentials

This is needed in order to allow terraform to access GCP.

Create a folder called `credentials/` and create a file called `terraform-gke-keyfile.json`. Then ask Julian for contents. 
It's just easier this way due to small contributer

### Initialize

Setup remote backend state connection. Downloads providers, plugins and dependencies to `.terraform/`

```
terraform init
```

### Plan

Check changes to be made to GCP before applying them.

```
terraform plan
```

### Apply

Apply the changes to GPC. This may take a while (~8mins)

```
terraform apply
```

