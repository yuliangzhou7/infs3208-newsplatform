provider "google" {
  version     = "2.11.0"
  credentials = "${file(var.credentials)}"
  project     = var.project_id
  region      = var.region
}

