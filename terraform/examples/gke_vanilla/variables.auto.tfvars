credentials        = "./credentials/terraform-gke-keyfile.json"
project_id         = "infs3208-newsplatform"
region             = "australia-southeast1-a"
cluster_name       = "my-gke-cluster"
node_pool_name     = "my-node-pool"
machine_type       = "g1-small"
min_count          = 1
max_count          = 3
disk_size_gb       = 10
service_account    = "terraform-gke@infs3208-newsplatform.iam.gserviceaccount.com"
initial_node_count = 3
username           = "emperortrump"
password           = "nomorefakenews3208" 

