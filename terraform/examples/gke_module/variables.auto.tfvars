credentials        = "./credentials/terraform-gke-keyfile.json"
project_id         = "infs3208-newsplatform"
region             = "australia-southeast1"
zones              = ["australia-southeast1-a"]
name               = "gke-cluster"
machine_type       = "n1-standard-1"
min_count          = 1
max_count          = 3
disk_size_gb       = 10
service_account    = "terraform-gke@infs3208-newsplatform.iam.gserviceaccount.com"
initial_node_count = 3

