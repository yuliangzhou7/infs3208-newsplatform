output "master-endpoint" {
  description = "The IP address of this cluster's VM master."
  value = "${google_compute_instance.vm_instance_master.network_interface[0].access_config[0].nat_ip}"
}
