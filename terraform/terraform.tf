terraform {
  backend "gcs" {
    credentials = "./credentials/terraform-gke-keyfile.json"
    bucket      = "infs3208-newsplatofrm-terraform-state-gke"
    prefix      = "terraform/state"
  }
}
