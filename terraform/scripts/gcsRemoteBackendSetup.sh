#!/bin/bash

# Note that this script is used to setup remote backend for terraform state and should only be run once.

export PROJECT_NAME=infs3208-newsplatform
export SERVICE_ACCOUNT_NAME=terraform-gke
export BUCKET_NAME=infs3208-newsplatofrm-terraform-state-gke
export LOCATION=australia-southeast1

# Create bucket
gsutil mb -p ${PROJECT_NAME} -c regional -l ${LOCATION} gs://${BUCKET_NAME}/

# Activate bucket versioning for state recovery
gsutil versioning set on gs://${BUCKET_NAME}/

# Grant bucket access to service account
gsutil iam ch serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_NAME}.iam.gserviceaccount.com:legacyBucketWriter gs://${BUCKET_NAME}/

