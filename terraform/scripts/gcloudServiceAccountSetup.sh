#!/bin/bash

# Note that this is a single time script used to create service account for the project. Don't need to run this again!

export PROJECT_NAME=infs3208-newsplatform
export SERVICE_ACCOUNT_NAME=terraform-gke

# Enable gcloud services
gcloud services enable compute.googleapis.com
gcloud services enable servicenetworking.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable container.googleapis.com

# Create service account
gcloud iam service-accounts create ${SERVICE_ACCOUNT_NAME}

# Add policies to service account
gcloud projects add-iam-policy-binding ${PROJECT_NAME} --member serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_NAME}.iam.gserviceaccount.com --role roles/container.admin
gcloud projects add-iam-policy-binding ${PROJECT_NAME} --member serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_NAME}.iam.gserviceaccount.com --role roles/compute.admin
gcloud projects add-iam-policy-binding ${PROJECT_NAME} --member serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_NAME}.iam.gserviceaccount.com --role roles/iam.serviceAccountUser
gcloud projects add-iam-policy-binding ${PROJECT_NAME} --member serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_NAME}.iam.gserviceaccount.com --role roles/resourcemanager.projectIamAdmin

# Create and download service account access key (DO NOT LOSE THIS KEY)
gcloud iam service-accounts keys create terraform-gke-keyfile.json --iam-account=${SERVICE_ACCOUNT_NAME}@${PROJECT_NAME}.iam.gserviceaccount.com

mkdir credentials
mv terraform-gke-keyfile.json credentials/

