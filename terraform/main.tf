# Master VM instance
resource "google_compute_instance" "vm_instance_master" {
  name         = "terraform-instance-master"
  machine_type = var.machine_types["master"]
  zone         = var.zone
  tags         = ["master", "http", "jupyter"]

  provisioner "local-exec" {
    command = "echo ${google_compute_instance.vm_instance_master.name}: ${google_compute_instance.vm_instance_master.network_interface[0].access_config[0].nat_ip} >> master_ip_address.txt"
  }

  allow_stopping_for_update = true
  
  boot_disk {
    initialize_params {
      image = var.disk_image
      size  = var.disk_size_gb
    }
  }

  network_interface {
    network    = module.network.network_name
    subnetwork = module.network.subnets_names[0]
    access_config {
      nat_ip = google_compute_address.vm_static_ip.address
    }
  }
}

# Slave VM instances
resource "google_compute_instance" "vm_instance_slave1" {
  name         = "terraform-instance-slave1"
  machine_type = var.machine_types["slave"]
  zone         = var.zone
  tags         = ["slave1", "http"]

  allow_stopping_for_update = true
  
  boot_disk {
    initialize_params {
      image = var.disk_image
      size  = var.disk_size_gb
    }
  }

  network_interface {
    network    = module.network.network_name
    subnetwork = module.network.subnets_names[0]
    access_config {
      # Ephemeral IP
      network_tier = "STANDARD"
    }
  }
}
resource "google_compute_instance" "vm_instance_slave2" {
  name         = "terraform-instance-slave2"
  machine_type = var.machine_types["slave"]
  zone         = var.zone
  tags         = ["slave2", "http"]

  allow_stopping_for_update = true
  
  boot_disk {
    initialize_params {
      image = var.disk_image
      size  = var.disk_size_gb
    }
  }

  network_interface {
    network    = module.network.network_name
    subnetwork = module.network.subnets_names[0]
    access_config {
      # Ephemeral IP
      network_tier = "STANDARD"
    }
  }
}

# Static ip for master node
resource "google_compute_address" "vm_static_ip" {
  name = "terraform-static-ip"
}

# Module for more custom networking
module "network" {
  source  = "terraform-google-modules/network/google"
  version = "1.1.0"

  network_name = "terraform-vpc-network"
  project_id   = var.project_id

  subnets = [
    {
      subnet_name   = "subnet-01"
      subnet_ip     = var.cidrs[0]
      subnet_region = var.region
    },
    {
      subnet_name   = "subnet-02"
      subnet_ip     = var.cidrs[1]
      subnet_region = var.region

      subnet_private_access = "true"
    },
  ]

  secondary_ranges = {
    subnet-01 = []
    subnet-02 = []
  }
}

# Firewall allow internal
resource "google_compute_firewall" "allow-internal" {
  name    = "terraform-allow-internal"
  network = "${module.network.network_name}"
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }
  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }
  source_ranges = [
    "${var.cidrs[0]}",
    "${var.cidrs[1]}",
  ]
}

# Firewall allow ssh
resource "google_compute_firewall" "allow-ssh" {
  name    = "terraform-allow-ssh"
  network = "${module.network.network_name}"
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

# Firewall allow external http
resource "google_compute_firewall" "allow-http" {
  name    = "terraform-allow-http"
  network = "${module.network.network_name}"
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  target_tags = ["http"] 
}

# Firewall allow jupyter
resource "google_compute_firewall" "allow-jupyter" {
  name    = "terraform-allow-jupyter"
  network = "${module.network.network_name}"
  allow {
    protocol = "tcp"
    ports    = ["8888"]
  }
  target_tags = ["jupyter"] 
}
