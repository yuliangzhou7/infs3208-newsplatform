variable "credentials" {
  type        = string
  description = "Location of the credentials keyfile."
}

variable "project_id" {
  type        = string
  description = "The project ID to host the cluster in."
}

variable "region" {
  type        = string
  description = "The region to host the cluster in."
}

variable "zone" {
  type        = string
  description = "The zone to host the cluster in."
}

variable "machine_types" {
  type = "map"
  description = "Type of the node compute engines."
  default = {
    "prod" = "n1-standard-1"
    "dev" = "g1-small"
  }
}

variable "disk_image" {
  type        = string
  description = "Disk image type"
}

variable "disk_size_gb" {
  type        = number
  description = "Size of the node's disk."
}

variable "cidrs" { default = [] }
