## Demo Presentation

[Youtube: INFS3208 Cloud Computing | Group 69 | News Platform](https://www.youtube.com/watch?v=wsyIF7ds-54)

## How to Run Locally

1. Build images
```
docker-compose build
```

Run all images
```
docker-compose up
```

## Run in Production 

## Terraform 

```
terraform init
terraform plan
terraform apply
terraform destroy # Don't use this unless necessary
```

---

## Useful Commands

Quick access node ssh commands:
```
gcloud beta compute --project "infs3208-newsplatform" ssh --zone "us-central1-a" "default@terraform-instance-master"
gcloud beta compute --project "infs3208-newsplatform" ssh --zone "us-central1-a" "default@terraform-instance-slave1"
gcloud beta compute --project "infs3208-newsplatform" ssh --zone "us-central1-a" "default@terraform-instance-slave2"
```

Get Jupyter Token from container:
```
# You will need to be ssh'd into the machine running the jupyter container
docker exec -it `docker ps -qf name=jupyter-pyspark` jupyter notebook list
```

cqlsh into cassandra node:
```
docker exec -it `docker ps -qf name=cassandra` cqlsh
```

Cassandra healthchecking:
```
docker exec `docker ps -qf name=cassandra` nodetool status
```

---

## Configuration

### Ports exposed to host machine

Accessing client and flask-server. Flask routed based on url starting with "/api/".
`nginx: 4000`
For accessing cqlsh from host (outside of container network)
`cassandra0: 9042`
Jupyter notebook interface w/ Pyspark
`jupyter-pyspark: 8888`

### FAQ

1. Why does building the flask server container freeze?

    Cassandra driver for python takes a quite a while to download (5-10mins). To speed it up increase resources available to Docker Engine. This can be done through:
    **Docker Settings**: Open *Preferences → Advanced → CPUs & Memory*
    Regardless, should still take a few minutes so go take a break :)

---

## References

- [Python-Cassandra Connector Docs](https://docs.datastax.com/en/developer/python-driver/3.19/getting_started/)
- [Flask-RESTful Getting Started Docs](https://flask-restful.readthedocs.io/en/0.3.6/quickstart.html)
- [Eslint & Prettier Integration in React](https://medium.com/quick-code/how-to-integrate-eslint-prettier-in-react-6efbd206d5c4)