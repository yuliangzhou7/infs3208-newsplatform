#!/bin/bash

if [ $# -eq 0 ]
then
    printf "Please specify master or slave[1/2]...\n"
    exit 1
fi

if [ "$1" = "master" ]; then
    printf "Just use docker-compose\n"
    # docker run --name cassandra0 \
    #     -p 9042:9042 -p 7000:7000 \
    #     -v /home/$USER/data/cassandra:/var/lib/cassandra \
    #     -e CASSANDRA_BROADCAST_ADDRESS=10.0.0.5 \
    #     -d cassandra:3.11.4
elif [ "$1" = "slave1" ]; then
    printf "Starting cassandra slave1...\n"
    docker run --name cassandra1 \
        -p 9042:9042 -p 7000:7000 \
        -v /home/$USER/data/cassandra:/var/lib/cassandra \
        -e CASSANDRA_SEEDS=10.0.0.5 \
        -e CASSANDRA_BROADCAST_ADDRESS=10.0.0.3 \
        -d cassandra:3.11.4
elif [ "$1" = "slave2" ]; then
    printf "Starting cassandra slave2...\n"
    docker run --name cassandra2 \
        -p 9042:9042 -p 7000:7000 \
        -v /home/$USER/data/cassandra:/var/lib/cassandra \
        -e CASSANDRA_SEEDS=10.0.0.5 \
        -e CASSANDRA_BROADCAST_ADDRESS=10.0.0.4 \
        -d cassandra:3.11.4
else
    printf "Incorrect option provided. Aborted...\n"
    exit 1
fi
