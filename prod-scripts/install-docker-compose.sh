#!/bin/bash

# Installs docker-compose in GCP where access to /usr/local/bin/ is read only
docker pull docker/compose:1.24.1

echo alias docker-compose="'"'docker run --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v "$PWD:$PWD" \
    -w="$PWD" \
    docker/compose:1.24.1'"'" >> ~/.bashrc

source ~/.bashrc
