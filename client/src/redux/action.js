// Action Types
export const SET_USER_ID = "SET_USER_ID"
export const SET_USER_NAME = "SET_USER_NAME"

export const setUserId = id => ({
  type: "SET_USER_ID",
  id: id
})

export const setUserName = name => ({
  type: "SET_USER_NAME",
  name: name
})