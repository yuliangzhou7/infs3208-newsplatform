import {
  SET_USER_ID,
  SET_USER_NAME
} from "./action"

const initialState = {
  id: null,
  name: ""
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    
    case SET_USER_ID:
      return {
        ...state,
        id: action.id
      }

    case SET_USER_NAME:
      return {
        ...state,
        name: action.name
      }

    default:
      return state
  }
}

export default reducer