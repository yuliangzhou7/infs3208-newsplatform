import React from 'react';

// For Material UI Components
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

// For Material UI Component CSS
const useStyles = makeStyles(theme => ({

  edit_article_title_input: {
    width: "100%",
  },

  edit_article_title_input_text: {
    fontSize: 30,
  },

  underline:{
    '&:after': {
      borderBottom:'2px solid #000000',
    },
  }
}));

// Title Input Component
function TitleInput(props) {
  const classes = useStyles();

  return (
    <TextField
      className={classes.edit_article_title_input}
      InputProps={{
        classes: {
          input: classes.edit_article_title_input_text,
          underline: classes.underline,
        }
      }}
      onChange={props.onChange}
    />    
  )
}

export default TitleInput;