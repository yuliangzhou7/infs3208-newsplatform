import React from 'react';

// For Material UI Components
import { makeStyles, withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

// For Material UI Component CSS
const useStyles = makeStyles(theme => ({

  edit_article_contents_input: {
    width: "100%",
  },
}));

// For Custome CSS TextField
const ContentsTextField = withStyles({
  root: {
   '& .MuiOutlinedInput-root': {
     '&.Mui-focused fieldset': {
       borderColor: '#000000',
     },
   },
  },
})(TextField);

function ContentsInput(props) {
  const classes = useStyles();

  return (
    <ContentsTextField 
      margin="normal"
      multiline
      variant="outlined"
      className={classes.edit_article_contents_input}
      inputProps={{
        style: {
          minHeight: 300,
        },
      }}
      onChange={props.onChange}
    />
  )
}

export default ContentsInput;