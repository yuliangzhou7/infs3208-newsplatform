import React from 'react';
import axios from 'axios'

import RateInput from "./rate"
import { connect } from 'react-redux'


// Import CSS
import "../../assets/css/article/viewArticle.css"

class ViewArticleComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title: "",
      // sub_title: "",
      tags: [],
      author: "",
      contents: "",
      average_rating: 0,
      rate: 0
    };

    this.setRate = this.setRate.bind(this)
    this.setRateDOM = this.setRateDOM.bind(this)
  }

  componentDidMount() {

    // Call API to get the article
    const article_id = this.props.article_id
    const user_id = this.props.id

    if(user_id === null) {
      axios.get('/api/articles/' + article_id).then(r => {
        const article = r.data
        this.setState({
          title: article.title,
          tags: article.tags,
          author: article.author_name,
          contents: article.body,
          average_rating: article.average_rating
        })
      })
    }
    else {
      axios.get('/api/articles/' + article_id + '?user_id=' + user_id).then(r => {
        const article = r.data
        this.setState({
          title: article.title,
          tags: article.tags,
          author: article.author_name,
          contents: article.body,
          average_rating: article.average_rating
        })
      })
    }

    
  }

  setTagsDOM() {
    let tags = []

    this.state.tags.forEach(tag => {
      tags.push(
        <div key={tag} className="view-article-tag">{tag}</div>
      )
    })

    return tags
  }

  setRate(rate) {
    this.setState({
      rate: rate
    })

    const article_id = this.props.article_id
    const user_id = this.props.id

    axios.post('/api/articles/' + article_id + '/rate', {
      user_id: user_id,
      rating: rate
    })
  }

  setRateDOM() {
    const user_id = this.props.id

    if(user_id) {
      return (
        <div className="view-article-rate-input">
          <RateInput onClick={this.setRate} value={this.state.rate}/>
        </div>
      )
    }
    else {
      return null
    }
  }

  render() {
    return (
      <div className="view-article">
        <div className="view-article-title">{this.state.title}</div>
        {/* <div className="view-article-sub-title">{this.state.sub_title}</div> */}
        <div className="view-article-tags">{this.setTagsDOM()}</div>
        <div className="view-article-author">{this.state.author}</div>
        <div className="view-article-contents">{this.state.contents}</div>
        <div className="view-article-average-rate">Average rate: {this.state.average_rating}</div>
        {/* <div className="view-article-rate-input">
          <RateInput onClick={this.setRate} value={this.state.rate}/>
        </div> */}
        {this.setRateDOM()}
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { id } = state
  return { id }
}


export default connect(mapStateToProps)(ViewArticleComponent)