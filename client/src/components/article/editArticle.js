import React from 'react';

// Components
import TitleInput from './titleInput'
// import SubTitleInput from './subTitleInput'
import ContentsInput from './contentsInput'
import UploadButton from './uploadButton'
import TagsInput from './tagsInput'
import axios from 'axios'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';


// Import CSS
import "../../assets/css/article/editArticle.css"

class EditArticleComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title: "",
      // sub_title: "",
      tags: [],
      contents: ""
    };

    // Bind Functions
    this.upload = this.upload.bind(this)
    this.setTitle = this.setTitle.bind(this)
    // this.setSubTitle = this.setSubTitle.bind(this)
    this.setTags = this.setTags.bind(this)
    this.setContents = this.setContents.bind(this)
  }

  upload() {
    const article_id = this.props.article_id
    const title = this.state.title
    const tags = this.state.tags
    const contents = this.state.contents
    const user_id = this.props.id
    const user_name = this.props.name

    // Create Article
    if(article_id === "new") {
      axios.post('/api/articles', {
        author_id: user_id,
        author_name: user_name,
        title: title,
        body: contents,
        tags: tags
      }).then(r => {
        this.nextPath('/')
      })
    }
    // Update Article 
    else {
      // axios.put('/api/articles/' + article_id, {
      //   id: article_id,

      // })
    }
  }

  setTitle(event) {
    const new_title = event.target.value
    this.setState({
      title: new_title
    })
  }

  // setSubTitle(event) {
  //   const new_sub_title = event.target.value
  //   this.setState({
  //     sub_title: new_sub_title
  //   })
  // }

  setTags(tags) {
    this.setState({
      tags: tags
    })
  }

  setContents(event) {
    const new_contents = event.target.value
    this.setState({
      contents: new_contents
    })
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="edit-article">

        {/* Title */}
        <div className="edit-article-title">
          <div className="edit-article-components-header">Title</div>
          <TitleInput onChange={this.setTitle}/>
        </div>

        {/* Sub title */}
        {/* <div className="edit-article-sub-title">
          <div className="edit-article-components-header">Sub Title</div>
          <SubTitleInput onChange={this.setSubTitle}/>
        </div> */}

        {/* Tags */}
        <div className="edit-article-tags">
          <div className="edit-article-components-header">Tags</div>
          <TagsInput setTags={this.setTags}/>
        </div>

        {/* Contents */}
        <div className="edit-article-contents">
          <div className="edit-article-components-header">Contents</div>
          <ContentsInput onChange={this.setContents}/>
        </div>

        {/* Upload Button */}
        <UploadButton onClick={this.upload}/>
      </div>
    )
  }
}


const mapStateToProps = state => {
  const { id, name } = state
  return { id, name }
}


export default connect(mapStateToProps)(withRouter(EditArticleComponent))

