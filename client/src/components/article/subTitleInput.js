import React from 'react';

// For Material UI Components
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

// For Material UI Component CSS
const useStyles = makeStyles(theme => ({

  edit_article_title_input: {
    width: "100%",
  },

  edit_article_sub_title_input_text: {
    fontSize: 20,
  },

  underline:{
    '&:after': {
      borderBottom:'2px solid #000000',
    },
  }
}));

function SubTitleInput(props) {
  const classes = useStyles();

  return (
    <TextField
      className={classes.edit_article_title_input}
      InputProps={{
        classes: {
          input: classes.edit_article_sub_title_input_text,
          underline: classes.underline,
        }
      }}
      rowsMax="4"
      multiline
      onChange={props.onChange}
    />    
  )
}

export default SubTitleInput;