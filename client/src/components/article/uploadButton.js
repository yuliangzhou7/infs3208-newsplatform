import React from 'react';

// For Material UI Components
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

// For Material UI Component CSS
const useStyles = makeStyles(theme => ({

  upload_button: {
    marginTop: 20,
    marginBottom: 50
  }
}));

// For Custome CSS Button
const ColorButton = withStyles(theme => ({
  root: {
    color: "#FFFFFF",
    backgroundColor: "#24ABC0",
    '&:hover': {
      backgroundColor: "#5CE2E9",
    },
  },
}))(Button);

// Upload Button Component
function UploadButton(props) {
  const classes = useStyles();

  return (
    <ColorButton 
      variant="contained" 
      className={classes.upload_button}
      onClick={props.onClick}
    >
      Upload
    </ColorButton>
  )
}

export default UploadButton;