import React from 'react';
import CancelIcon from '@material-ui/icons/Cancel';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';

import '../../assets/css/article/tagsInput.css'
 
class TagsInputComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tags: [],
      new_tag: ""
    };
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAddition = this.handleAddition.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleDelete(delete_tag) {
    const { tags } = this.state;
    this.setState({
     tags: tags.filter(tag => tag !== delete_tag),
    });
    this.props.setTags(tags.filter(tag => tag !== delete_tag))
  }

  handleAddition() {
    let tags = this.state.tags
    const new_tag = this.state.new_tag

    let check = true

    tags.forEach(tag => {
      if (tag === new_tag) {
        check = false
        return
      }
    })

    if (check) {
      tags.push(new_tag)
      this.setState(state => ({ 
        tags: tags,
        new_tag: ""
      }));
      this.props.setTags(tags)
    }
  }

  handleChange(e) {
    this.setState({new_tag: e.target.value});
  }

  setTagsDOM() {
    let tags = []

    this.state.tags.forEach(tag => {
      tags.push(
        <div key={tag} className="edit-article-tag">
          <span>{tag}</span>
          <CancelIcon onClick={() => this.handleDelete(tag)} />
        </div>
      )
    })

    return tags
  }

  render() {
    return (
      <div>
        <div className="edit-article-tags-list">
          {this.setTagsDOM()}
        </div>
        <div className="edit-article-add-tag">
          <Input value={this.state.new_tag} onChange={this.handleChange}/>
          <Button variant="contained" onClick={this.handleAddition}>
            Add
          </Button>
        </div>
      </div>
      
    )
  }
}

export default TagsInputComponent