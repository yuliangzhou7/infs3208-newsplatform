import React from 'react';
import { RadialChart, XYPlot, VerticalBarSeries, XAxis, YAxis, LabelSeries } from 'react-vis'
import axios from 'axios'

// Import CSS
import "../../assets/css/dashboard/global.css"

class GlobalPage extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      sample_data: [{angle: 1, label: "tag1"}, {angle: 5, label: "tag2"}, {angle: 2, label: "tag3"}],
      labelsStyle: {
        fontSize: 24,
        fill: "#FFFFFF"
      },
      popular_tags: [],
      popular_authors: [],
      popular_articles: []
    }
  }

  componentDidMount() {

    // Call API Get the dashboard data
    axios.get('/api/dashboard?type=tags').then(r => {
      const popular_tags = r.data.tag_data
      this.setState({
        popular_tags: popular_tags
      })
    })

    axios.get('/api/dashboard?type=authors').then(r => {
      
      const popular_authors = r.data.author_data
      this.setState({
        popular_authors: this.getBarChartData(popular_authors),
      })
    })
    
    // // Most popular tags
    // const popular_tags = [{angle: 10, label: "tag1"}, {angle: 8, label: "tag2"}, {angle: 6, label: "tag3"}, {angle: 4, label: "tag4"}, {angle: 2, label: "tag5"}]

    // // Most popular authors
    // const popular_authors = [
    //   {id: "author-id-1", data: {y: 10, x: "author 1 name"}}, 
    //   {id: "author-id-2", data: {y: 7, x: "author 2 name"}}, 
    //   {id: "author-id-3", data: {y: 5, x: "author 3 name"}},
    //   {id: "author-id-4", data: {y: 3, x: "author 4 name"}},
    //   {id: "author-id-1", data: {y: 1, x: "author 5 name"}}
    // ]

    // Most popular articles
    const popular_articles = [
      {id: "article-id-1", data: {y: 10, x: "article 1 name"}}, 
      {id: "article-id-2", data: {y: 7, x: "article 2 name"}}, 
      {id: "article-id-3", data: {y: 5, x: "article 3 name"}},
      {id: "article-id-4", data: {y: 3, x: "article 4 name"}},
      {id: "article-id-1", data: {y: 1, x: "article 5 name"}}
    ]

    this.setState({
      // popular_tags: popular_tags,
      // popular_authors: this.getBarChartData(popular_authors),
      popular_articles: this.getBarChartData(popular_articles)
    })
  }

  // Get Chart Data from API
  getBarChartData(list) {
    let return_array = []

    list.forEach(item => {
      return_array.push(item.data)
    })
    
    return return_array
  }


  render() {
    return(
      <div className="global-page">
        <div className="tags">
        <div className="chart-title">Most Popular Tags</div>
          <div className="chart-container">
            <RadialChart 
              data={this.state.popular_tags} 
              width={300} 
              height={300} 
              showLabels={true}
              labelsStyle={this.state.labelsStyle}
              className="chart"
            >
            </RadialChart>
          </div>
        </div>

        <div className="authors">
          <div className="chart-title">Most Popular Authors</div>
          <div className="chart-container">
            <XYPlot 
              xType="ordinal" 
              width={700} 
              height={300} 
              yDomain={[0, 50]}
            >
              <XAxis />
              <YAxis />
              <VerticalBarSeries
                data={this.state.popular_authors}
              />
              <LabelSeries
                data={this.state.popular_authors.map(obj => {
                  return { ...obj, label: obj.y.toString() }
                })}
                labelAnchorX="middle"
                labelAnchorY="text-after-edge"
              />
            </XYPlot>
          </div>
        </div>

        {/* <div className="authors">
          <div className="chart-title">Most Popular Articles</div>
          <div className="chart-container">
            <XYPlot 
              xType="ordinal" 
              width={700} 
              height={300} 
              yDomain={[0, 50]}
            >
              <XAxis />
              <YAxis />
              <VerticalBarSeries
                data={this.state.popular_articles}
              />
              <LabelSeries
                data={this.state.popular_articles.map(obj => {
                  return { ...obj, label: obj.y.toString() }
                })}
                labelAnchorX="middle"
                labelAnchorY="text-after-edge"
              />
            </XYPlot>
          </div>
        </div> */}
      </div>


    )
  }
}

export default GlobalPage

