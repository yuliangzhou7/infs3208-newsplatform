import React from 'react';
import { RadialChart, XYPlot, VerticalBarSeries, XAxis, YAxis, LabelSeries } from 'react-vis'
import axios from 'axios'

// Import CSS
import "../../assets/css/dashboard/personal.css"

class PersonalPage extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      sample_data: [{angle: 1, label: "tag1"}, {angle: 5, label: "tag2"}, {angle: 2, label: "tag3"}],
      labelsStyle: {
        fontSize: 24,
        fill: "#FFFFFF"
      },
      most_viewed_tags: [],
      most_viewed_authors: []
    }
  }

  componentDidMount() {

    const user_id = this.props.user_id

    // Call API Get the dashboard data
    if(user_id !== undefined) {
      axios.get('/api/dashboard/' + user_id + '?type=tags').then(r => {
        const tags = r.data.tag_data
        this.setState({
          most_viewed_tags: tags
        })
      })
      axios.get('/api/dashboard/' + user_id + '?type=authors').then(r => {
        const authors = r.data.author_data
        let most_viewed_authors = []
        authors.forEach(author => {
          most_viewed_authors.push(author.data)
        })
        this.setState({
          most_viewed_authors: most_viewed_authors,
        })
      })
    }


    let history_articles = []
    let tags = ["tag0", "tag1", "tag2", "tag3", "tag4"]
    let authors = ["author0", "author1", "author2", "author3", "author4"]

    for(let i = 0; i < 50; i++) {
      let random = Math.floor(Math.random() * 5);
      let tag = tags[random]
      let author = authors[random]

      let sample_article = {
        id: "id-" + i,
        title: "Article Title",
        subTitle: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia",
        author: author,
        author_id: "author-id-01",
        tags: [tag],
        contexts: "Contexts"
      }
      history_articles.push(sample_article)
    }

    this.getMostViewedData(history_articles)
  }

  getMostViewedData(articles) {
    let authors = {}
    let tags = {}

    articles.forEach(article => {
      let author = article.author
      let tags_in_article = article.tags

      let author_counter = authors[author]

      if (author_counter) {
        authors[author] = author_counter + 1
      }
      else {
        authors[author] = 1
      }

      tags_in_article.forEach(tag => {
        let tag_counter = tags[tag]

        if (tag_counter) {
          tags[tag] = tag_counter + 1
        }
        else {
          tags[tag] = 1
        }
      })
    })

    // let most_viewed_tags = this.getPieChartData(tags)
    // let most_viewed_authors = this.getBarChartData(authors)
    // console.log(most_viewed_authors)

    // this.setState({
    //   most_viewed_authors: most_viewed_authors,
    //   most_viewed_tags: most_viewed_tags
    // })
  }

  getPieChartData(data_object) {
    let return_array = []

    let keys = Object.keys(data_object)
    keys.forEach(key => {
      let counter = data_object[key]
      let item = {
        angle: counter,
        label: key
      }
      return_array.push(item)
    })

    return return_array
  }

  getBarChartData(data_object) {
    let return_array = []

    let keys = Object.keys(data_object)
    keys.forEach(key => {
      let counter = data_object[key]
      let item = {
        y: counter,
        x: key
      }
      return_array.push(item)
    })
    
    return return_array
  }

  render() {
    return(
      <div className="personal-page">
        <div className="tags">
          <div className="chart-title">My Most Viewed Tags</div>
          <div className="chart-container">
            <RadialChart 
              data={this.state.most_viewed_tags} 
              width={300} 
              height={300} 
              showLabels={true}
              labelsStyle={this.state.labelsStyle}
              className="chart"
            >
            </RadialChart>
          </div>
        </div>

        <div className="authors">
          <div className="chart-title">My Most Viewed Authors</div>
          <div className="chart-container">
            <XYPlot 
              xType="ordinal" 
              width={500} 
              height={300} 
              yDomain={[0, 50]}
            >
              <XAxis />
              <YAxis />
              <VerticalBarSeries
                data={this.state.most_viewed_authors}
              />
              <LabelSeries
                data={this.state.most_viewed_authors.map(obj => {
                  return { ...obj, label: obj.y.toString() }
                })}
                labelAnchorX="middle"
                labelAnchorY="text-after-edge"
              />
            </XYPlot>
          </div>
        </div>
      </div>
    )
  }
}

export default PersonalPage

