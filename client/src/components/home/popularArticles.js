import React from 'react';
import PersonIcon from '@material-ui/icons/Person';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

// Import CSS
import "../../assets/css/home/popularArticles.css"

class PoplularArticlesComponent extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      popularArticles: []
    }
  }

  componentDidMount() {

    // Get Popular Articles from API
    axios.get('/api/popular/articles').then(r => {
      const articles = r.data.articles
      this.setState(state => ({
        popularArticles: articles
      }))
    })

    // let sample_popular_articles = []

    // for(let i = 0; i < 5; i++) {
    //   let sample_article = {
    //     id: "id-" + i,
    //     title: "Article Title",
    //     subTitle: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia",
    //     author: "Author Name",
    //     author_id: "author-id-02",
    //     tags: ["tag1", "tag2"],
    //   }
  
    //   sample_popular_articles.push(sample_article)
    // }

    // this.setState(state => ({
    //   popularArticles: sample_popular_articles
    // }))
  }

  setPopularArticlesDOM() {
    let popular_articles = []

    this.state.popularArticles.forEach(article => {
      popular_articles.push(
        <div key={article.id} className="popular-article">
          <div className="popular-article-title" onClick={() => this.nextPath('/article/' + article.id)}>{article.title}</div>
          <div className="popular-article-tags">{this.setTagsDOM(article.tags)}</div>
          {/* <div className="popular-article-sub-title" onClick={() => this.nextPath('/article/' + article.id)}>{article.subTitle}</div> */}
          <div className="author" onClick={() => this.nextPath('/article/' + article.id)}>
            <PersonIcon style={{ fontSize: 24 }} />
            <span className="author-name">{article.author_name}</span>
          </div>
        </div>
      )
    })

    return popular_articles
  }

  setTagsDOM(tags) {
    let tags_dom = []

    tags.forEach(tag => {
      tags_dom.push(
        <div key={tag}  className="popular-article-tag">{tag}</div>
      )
    })

    return tags_dom
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="popular-articles">
        <div className="title">Popular Articles</div>

        {this.setPopularArticlesDOM()}
      </div>
    )
  }
}

export default withRouter(PoplularArticlesComponent)

