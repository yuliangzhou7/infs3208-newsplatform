import React from 'react';
import axios from 'axios';

// Import CSS
import "../../assets/css/home/poplularTags.css"

class PopularTagsComponent extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      popularTags: [],
    }
  }

  setPopularTagsDOM() {
    let popularTags = []
    let rank = 1

    this.state.popularTags.forEach(popularTag => {
      popularTags.push(
        <div key={popularTag} className="popular-tag">
          <span className="rank">{rank}.</span>
          <span className="name">#{popularTag}</span>
        </div>
      )
      rank += 1
    })

    return popularTags
  }

  componentDidMount() {
    // Get Popular Tags from API
    axios.get('/api/popular/tags').then(r => {
      const tags = r.data.tags

      let popularTags = []

      tags.forEach(tag => {
        popularTags.push(tag.name)
      })
      this.setState(({
        popularTags: popularTags
      }))
    })
  }

  render() {
    return (
      <div className="popular-tags">
        <div className="title">
          Popular Tags
        </div>

        {this.setPopularTagsDOM()}
      </div>
    )
  }
}

export default PopularTagsComponent

