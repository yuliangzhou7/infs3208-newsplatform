import React from 'react';

// Import CSS
import "../../assets/css/home/subheader.css"

class SubHeaderComponent extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      tags: ['Tag 1', 'Tag 2', 'Tag 3', 'Tag 4', 'Tag 5'],
      selected: 'All'
    }
  }

  setTagesDOM() {
    let tags = []

    this.state.tags.forEach(tag => {
      tags.push(<span key={tag} className={this.setTagStyle(tag)} onClick={() => this.selectTag(tag)}>{tag}</span>)
    })

    return tags
  }

  setTagStyle(tag) {
    if(tag === this.state.selected) {
      return "tag active"
    }
    else {
      return "tag"
    }
  }

  selectTag(tag) {
    this.setState(state => ({
      selected: tag
    }))
  }

  render() {
    return(
      <div className="sub-header">
        <span 
          className={this.setTagStyle("All")}
          onClick={() => this.selectTag("All")}
        >All</span>

        {this.setTagesDOM()}
      </div>
    )
  }
}

export default SubHeaderComponent