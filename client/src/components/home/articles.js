import React from 'react';
import PersonIcon from '@material-ui/icons/Person';
import { withRouter } from 'react-router-dom';
import axios from 'axios';


// Import CSS
import "../../assets/css/home/articles.css"

class ArticlesComponent extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      articles: []
    }
  }

  // For Create Sample Articles
  componentDidMount() {

    // Get Articles from API
    axios.get('/api/articles').then(r => {
      const articles = r.data.articles      
      this.setState(state => ({
        articles: articles
      }))
    })

    // let sample_articles = []

    // for(let i = 0; i < 10; i++) {

    //   let sample_article = {
    //     id: "id-" + i,
    //     title: "Article Title",
    //     subTitle: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia",
    //     author: "Author Name",
    //     author_id: "author-id-01",
    //     tags: ["tag1", "tag2"],
    //     contexts: "Contexts"
    //   }

    //   sample_articles.push(sample_article)
    // }

    // this.setState(state => ({
    //   articles: sample_articles
    // }))
  }

  setArticlesDOM() {
    let articles = []

    this.state.articles.forEach(article => {
      articles.push(
        <div key={article.id} className="home-article">
          <div className="article-title" onClick={() => this.nextPath('/article/' + article.id)}>{article.title}</div>
          <div className="article-tags">{this.setTagsDOM(article.tags)}</div>
          {/* <div className="article-sub-title" onClick={() => this.nextPath('/article/' + article.id)}>{article.subTitle}</div> */}
          <div className="author" onClick={() => this.nextPath('/article/' + article.id)}>
            <PersonIcon style={{ fontSize: 24 }} />
            <span className="author-name">{article.author_name}</span>
          </div>
        </div>
      )
    })

    return articles
  }

  setTagsDOM(tags) {
    let tags_dom = []

    tags.forEach(tag => {
      tags_dom.push(
        <div key={tag}  className="article-tag">{tag}</div>
      )
    })

    return tags_dom
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="articles">
        {this.setArticlesDOM()}
      </div>
    )
  }
}

export default withRouter(ArticlesComponent)

