import React from 'react';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'

import { connect } from 'react-redux'

// Import CSS
import "../../assets/css/common/header.css"

// For Material UI Component CSS
const useStyles = makeStyles(theme => ({
  upload_button: {
    backgroundColor: "#FFFFFF",
    color: "#24ABC0"
  }
}));

// Upload Button Componenet
function UploadButton(props) {
  const classes = useStyles();
  const user_id = props.user_id
  // console.log(user_id)

  if(user_id) {
    return (
      <Link to="/article/new" className="upload-link">
        <Button variant="contained" className={classes.upload_button}>
          Create Article
        </Button>
      </Link>
    )
  }
  else {
    return null
  }
}

// Set User Icon
function SetUserIcon(props) {
  const classes = useStyles();
  const user_id = props.user_id

  if (user_id) {
    return (
      <Link to={"/user/" + user_id} className="user-link">
        <AccountCircleIcon className="header-icon" style={{ fontSize: 30 }} />
      </Link>
    )
  }
  else {
    return (
      <Link to="/login" className="login-link" style={{marginLeft: 'auto'}}>
        <Button variant="contained" className={classes.upload_button}>
          Log in
        </Button>
      </Link>
    )
  }
}


class HeaderComponent extends React.Component {

  constructor(props) {
    super(props)

    this.getDashboardLink = this.getDashboardLink.bind(this)
  }

  getDashboardLink() {
    if (this.props.id) {
      return "/dashboard/" + this.props.id
    }
    else {
      return "/dashboard"
    }
  }
  

  render() {
    return(
      <div className="header">
        <Link to="/" className="header-text1-link">
          <span className="header-text1">News Platform</span>
        </Link>

        <Link to="/" className="header-text2-link">
          <span className="header-text2">ARTICLES</span>
        </Link>

        <Link to="/authors" className="header-text2-link">
          <span className="header-text2">AUTHORS</span>
        </Link>
        {/* "/dashboard/" + this.props.user_id */}
        <Link to={this.getDashboardLink()} className="header-text2-link">
          <span className="header-text2">DASHBOARD</span>
        </Link>

        <Link to="/about" className="header-text2-link">
          <span className="header-text2">ABOUT</span>
        </Link>
        
        <UploadButton user_id={this.props.id}/>
        <SetUserIcon user_id={this.props.id} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { id } = state
  return { id }
}


export default connect(mapStateToProps)(HeaderComponent)