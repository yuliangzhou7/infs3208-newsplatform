import React from 'react';

// For Material UI Components
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

// For Material UI Component CSS
const useStyles = makeStyles(theme => ({

  user_name_input: {
    width: "100%",
  },

  underline:{
    '&:after': {
      borderBottom:'2px solid #000000',
    },
  }
}));

// Title Input Component
function UserEmail(props) {
  const classes = useStyles();

  return (
    <TextField
      className={classes.user_name_input}
      InputProps={{
        classes: {
          underline: classes.underline,
        }
      }}
      onChange={props.onChange}
      label="Email"
      value={props.value}
      disabled={props.readonly}
    />    
  )
}

export default UserEmail;