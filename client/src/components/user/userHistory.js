import React from 'react';
import PersonIcon from '@material-ui/icons/Person';
import {withRouter} from 'react-router-dom';


class UserHistory extends React.Component {
  constructor(props) {
    super(props)

    this.setArticlesDOM = this.setArticlesDOM.bind(this)
    this.setTagsDOM = this.setTagsDOM.bind(this)
  }

  setArticlesDOM() {
    let articles = []

    this.props.history_articles.forEach(article => {
      articles.push(
        <div key={article.article_id} className="home-article">
          <div className="article-title" onClick={() => this.nextPath('/article/' + article.article_id)}>{article.title}</div>
          <div className="article-tags">{this.setTagsDOM(article.tags)}</div>
          <div className="article-sub-title" onClick={() => this.nextPath('/article/' + article.article_id)}>{article.subTitle}</div>
          <div className="author" onClick={() => this.nextPath('/article/' + article.article_id)}>
            <PersonIcon style={{ fontSize: 24 }} />
            <span className="author-name">{article.author_name}</span>
          </div>
        </div>
      )
    })

    return articles
  }

  setTagsDOM(tags) {
    let tags_dom = []

    tags.forEach(tag => {
      tags_dom.push(
        <div key={tag}  className="article-tag">{tag}</div>
      )
    })

    return tags_dom
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="user-history-list">
        {this.setArticlesDOM()}
      </div>
    )
  }
}

export default withRouter(UserHistory)