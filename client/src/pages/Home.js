import React from 'react';
import Subheader from '../components/home/subheader'
import PopularTag from '../components/home/popularTags'
import Articles from "../components/home/articles"
import PopularArticles from "../components/home/popularArticles"

// Import CSS
import "../assets/css/home/home.css"

class HomePage extends React.Component {

  render() {
    return(
      <div className="home">
        <Subheader />

        <div className="main">
          <PopularTag />
          <Articles />
          <PopularArticles />
        </div>
      </div>
    )
  }
}

export default HomePage