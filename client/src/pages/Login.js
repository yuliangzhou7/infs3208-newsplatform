import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link, withRouter } from 'react-router-dom'

import { connect } from 'react-redux'
import { setUserId, setUserName } from '../redux/action'
import axios from 'axios';


// Import CSS
import "../assets/css/login/login.css"

// For Material UI Component CSS
const useStyles = makeStyles(theme => ({

  login_input: {
    width: 400,
  },

  login_button: {
    marginTop: 50
  }

}));

function EmailInput(props) {
  const classes = useStyles();

  return (
    <TextField
    label="Email"
    className={classes.login_input}
    onChange={props.onChange}
    margin="normal"
    />    
  )
}

function PasswordInput(props) {
  const classes = useStyles();

  return (
    <TextField
    label="Password"
    className={classes.login_input}
    onChange={props.onChange}
    type="password"
    margin="normal"
    />    
  )
}

function LoginButton(props) {
  const classes = useStyles();

  return (
    <Button 
     variant="contained" 
     color="primary"
     className={classes.login_button}
     onClick={props.onClick}
    >
      Login
    </Button>
  )
}

class LoginPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      password: ""
    }

    this.setEmail = this.setEmail.bind(this)
    this.setPassword = this.setPassword.bind(this)
    this.login = this.login.bind(this)
  }

  setEmail(event) {
    const email = event.target.value
    this.setState({
      email: email
    })
  }

  setPassword(event) {
    const password = event.target.value
    this.setState({
      password: password
    })
  }

  login() {
    const email = this.state.email
    const password = this.state.password

    axios.post('/api/login', {
      email: email,
      password: password
    }).then(r => {
      const id = r.data.token
      this.props.setUserId(id)

      axios.get('/api/users/' + id).then(r => {
        const name = r.data.name
        this.props.setUserName(name)
        this.nextPath('/')
      })
    })
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="login-page">
        <div className="login-title">Login</div>
        <EmailInput onChange={this.setEmail}/>
        <PasswordInput onChange={this.setPassword}/>
        <LoginButton onClick={this.login}/>
        <Link to="/signon" className="signon-link">Sign on</Link>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { id } = state
  return { id }
}

export default connect(mapStateToProps, {setUserId, setUserName})(withRouter(LoginPage))