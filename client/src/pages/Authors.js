import React from 'react';
import {withRouter} from 'react-router-dom';
import axios from 'axios';

// Import CSS
import "../assets/css/authors/authors.css"

class AuthorsPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      authors: [],
    };
  }

  componentDidMount() {

    // Get Authors From API
    axios.get('/api/authors').then(r => {
      const authors = r.data.authors
      this.setState({
        authors: authors
      })
    })

    // let authors = []

    // for(let i = 0; i < 20; i++) {
    //   const sample_author = {
    //     id: "author-id-" + i,
    //     name: "Author" + i
    //   }

    //   authors.push(sample_author)
    // }

    // this.setState({
    //   authors: authors
    // })
  }

  setAuthorsListDOM() {
    let authors = []

    this.state.authors.forEach(author => {
      authors.push(
        <div key={author.id} className="author" onClick={() => this.nextPath('/author/' + author.id)}>
          <div className="author-name">{author.name}</div>
        </div>
      )
    })

    return authors
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  render() {

    return(
      <div className="authors">
        <div className="authors-list">
          {this.setAuthorsListDOM()}
        </div>
      </div>
    )
  }
}

export default withRouter(AuthorsPage)