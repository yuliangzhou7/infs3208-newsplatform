import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link, withRouter } from 'react-router-dom'

import { connect } from 'react-redux'
import { setUserId, setUserName } from '../redux/action'
import axios from 'axios';


// Import CSS
// import "../assets/css/login/login.css"

const useStyles = makeStyles(theme => ({

  login_input: {
    width: 400,
  },

  login_button: {
    marginTop: 50
  }
}));

function NameInput(props) {
  const classes = useStyles();

  return (
    <TextField
    label="Name"
    className={classes.login_input}
    onChange={props.onChange}
    margin="normal"
    />    
  )
}

function EmailInput(props) {
  const classes = useStyles();

  return (
    <TextField
    label="Email"
    className={classes.login_input}
    onChange={props.onChange}
    margin="normal"
    />    
  )
}

function PasswordInput(props) {
  const classes = useStyles();

  return (
    <TextField
    label="Password"
    className={classes.login_input}
    onChange={props.onChange}
    type="password"
    margin="normal"
    />    
  )
}

function SignonButton(props) {
  const classes = useStyles();

  return (
    <Button 
     variant="contained" 
     color="primary"
     className={classes.login_button}
     onClick={props.onClick}
    >
      Sign on
    </Button>
  )
}

class SignonPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      email: "",
      password: ""
    }

    this.setName = this.setName.bind(this)
    this.setEmail = this.setEmail.bind(this)
    this.setPassword = this.setPassword.bind(this)
    this.signon = this.signon.bind(this)
  }

  setName(event) {
    const name = event.target.value
    this.setState({
      name: name
    })
  }

  setEmail(event) {
    const email = event.target.value
    this.setState({
      email: email
    })
  }

  setPassword(event) {
    const password = event.target.value
    this.setState({
      password: password
    })
  }

  signon() {
    const name = this.state.name
    const email = this.state.email
    const password = this.state.password

    axios.post('/api/signup', {
      email: email,
      name: name,
      password: password
    }).then(r => {
      const id = r.data.token
      this.props.setUserId(id)
      this.props.setUserName(name)
      this.nextPath('/')
    })
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="login-page">
        <div className="login-title">Sign On</div>
        <NameInput onChange={this.setName}/>
        <EmailInput onChange={this.setEmail}/>
        <PasswordInput onChange={this.setPassword}/>
        <SignonButton onClick={this.signon}/>
        <Link to="/login" className="signon-link">Login</Link>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { id } = state
  return { id }
}

export default connect(mapStateToProps, {setUserId, setUserName})(withRouter(SignonPage))
