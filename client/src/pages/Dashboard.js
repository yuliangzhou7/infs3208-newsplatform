import React from 'react';
import Tabs from '../components/dashboard/tabs'

// Import CSS
import "../assets/css/dashboard/dashboard.css"

class DashboardPage extends React.Component {

  componentDidMount() {

    // Call API Get the dashboard data

    
  }

  render() {
    return(
      <div className="dashboard-page">
        <Tabs user_id={this.props.match.params.id} />
      </div>
    )
  }
}

export default DashboardPage