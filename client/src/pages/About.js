import React from 'react';

// Import CSS
import "../assets/css/about/about.css"

class AboutPage extends React.Component {
  render() {
    return (
      <div className="about-page">
        <div className="about-page-main">
          <h1>About</h1>
          <div className="about-container">
          The main purpose of this application is to provide a platform for news agencies, journalists, and freelancers to share news articles, similar to the popular blog platform Medium, where users and critics can rate the credibility of the sources of the article. Writers with more reliable articles will get more highlights, while those who publish misleading or untruthful articles will marked as untrustworthy.
          </div>
        </div>
      </div>
    )
  }
}

export default AboutPage