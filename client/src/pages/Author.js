import React from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

// Import CSS
import "../assets/css/author/author.css"

class AuthorsPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      id: "",
      name: "",
      articles: []
    };

    this.setArticlesDOM = this.setArticlesDOM.bind(this)
    this.setTagsDOM = this.setTagsDOM.bind(this)
  }

  componentDidMount() {
    const id = this.props.match.params.id
    
    // Get Author Info From API
    axios.get('/api/authors/' + id).then(r => {
      const id = r.data.id
      const name = r.data.name
      const articles = r.data.articles

      this.setState({
        id: id,
        name: name,
        articles: articles
      })
    })

    // let articles = []

    // for(let i = 0; i < 5; i++) {
    //   const sample_article = {
    //     id: "article-id-" + i,
    //     title: "Sample Article",
    //     sub_title: "Sample Sub Title",
    //     tags: ["sample-1", "sample-2"]
    //   }

    //   articles.push(sample_article)
    // }

    // this.setState({
    //   id: id,
    //   name: "Sample Author",
    //   articles: articles
    // })
  }

  setArticlesDOM() {
    let articles = []

    this.state.articles.forEach(article => {
      articles.push(
        <div key={article.id} className="author-article" onClick={() => this.nextPath('/article/' + article.id)}>
          <div className="author-article-title">{article.title}</div>
          {/* <div className="author-article-sub-title">{article.sub_title}</div> */}
          <div className="author-article-tags">{this.setTagsDOM(article.tags)}</div>
        </div>
      )
    })

    return articles
  }

  setTagsDOM(tags) {
    let tagsDOM = []

    tags.forEach(tag => {
      tagsDOM.push(
        <div key={tag} className="author-tag">{tag}</div>
      )
    })

    return tagsDOM
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="author-page">
        <div className="author-name">
          {this.state.name}
        </div>
        <div className="author-articles">
          {this.setArticlesDOM()}
        </div>
      </div>
    )
  }
}

export default withRouter(AuthorsPage)