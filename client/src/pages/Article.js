import React from 'react';
import EditArticle from "../components/article/editArticle"
import ViewArticle from "../components/article/viewArticle"

// Import CSS
import "../assets/css/article/article.css"

class ArticlePage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      mode: "",
      url: ""
    };
  }

  // Set Mode (Upload, View, Edit)
  setMode() {
    const url = this.props.match.url
    const id = this.props.match.params.id
    const path = this.props.match.path

    // Escape Infinite Loop
    if (this.state.url === url) {
      return
    }
    else {
      this.setState({
        url: url
      })
    }
    // Upload Mode
    if (id === "new") {
      this.setState({
        mode: "upload"
      })
      return
    }

    // View Mode
    if (path === "/article/:id") {
      this.setState({
        mode: "view"
      })
      return
    }

    // Edit
    if (path === "/article/edit/:id") {
      this.setState({
        mode: "edit"
      })
      return
    }
  }

  componentDidMount() {
    this.setMode()
  }

  componentDidUpdate() { 
    this.setMode()
  } 

  render() {

    // Upload/Edit Mode
    if(this.state.mode === "upload" || this.state.mode === "edit") {
      return (
        <div className="article">
          <EditArticle article_id={this.props.match.params.id}/>
        </div>
      )
    }

    // View Mode
    else {
      return (
        <div className="article">
          <ViewArticle article_id={this.props.match.params.id}/>
        </div>
      )
    }
  }
}

export default ArticlePage