import React from 'react';
import SettingsIcon from '@material-ui/icons/Settings';
import Button from '@material-ui/core/Button';
import axios from 'axios'


// Import CSS
import "../assets/css/user/user.css"

import UserName from "../components/user/userName"
import UserEmail from "../components/user/userEmail"
// import UserPassword from "../components/user/userPassword"
import UserHistroy from "../components/user/userHistory"

class UserPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      id: "",
      name: "",
      email: "",
      password: "",
      history: [],
      setting: false
    };

    this.changeName = this.changeName.bind(this)
    this.changeEmail = this.changeEmail.bind(this)
    this.changePass = this.changePass.bind(this)
    this.toggleSetting = this.toggleSetting.bind(this)
    this.cancel = this.cancel.bind(this)
    this.update = this.update.bind(this)
  }

  componentDidMount() {
    const user_id = this.props.match.params.id

    // Call API to get user info by user id
    axios.get('/api/users/' + user_id).then(r => {
      const user = r.data
      const name = user.name
      const email = user.email

      this.setState({
        id: user_id,
        name: name,
        email: email,
      })
    })

    axios.get('/api/users/' + user_id + "/history").then(r => {
      const articles = r.data.articles
      this.setState({
        history: articles
      })
    })

    // let history = []

    // for(let i = 0; i  < 10; i++) {
    //   const sample_article = {
    //     id: "article-id-" + i,
    //     title: "Sample Article",
    //     subTitle: "Sample Sub Title",
    //     tags: ["sample-1", "sample-2"]
    //   }

    //   history.push(sample_article)
    // }

    // const sample_user = {
    //   id: user_id,
    //   name: "sample name",
    //   email: "sampleemail@gmail.com",
    //   password: "samplepassword",
    //   history: history
    // }

    // this.setState({
    //   id: sample_user.id,
    //   name: sample_user.name,
    //   email: sample_user.email,
    //   password: sample_user.password,
    //   history: sample_user.history
    // })
  }

  changeName(event) {
    const new_name = event.target.value
    this.setState({
      name: new_name
    })
  }

  changeEmail(event) {
    const new_email = event.target.value
    this.setState({
      email: new_email
    })
  }

  changePass(event) {
    const new_password = event.target.value
    this.setState({
      password: new_password
    })
  }

  toggleSetting() {
    this.setState({
      setting: !this.state.setting
    })
  }

  update() {
    // Call API to post new user data
    const user_id = this.state.id
    const name = this.state.name
    const email = this.state.email
    
    axios.put("/api/users/" + user_id, {
      id: user_id,
      email: email,
      name: name
    })

    this.setState({
      setting: false
    })
  }

  cancel() {
    // Call API to get user info by user id

    const sample_user = {
      id: this.state.id,
      name: "sample name",
      email: "sampleemail@gmail.com",
      password: "samplepassword",
    }

    this.setState({
      id: sample_user.id,
      name: sample_user.name,
      email: sample_user.email,
      password: sample_user.password,
    })

    this.setState({
      setting: false
    })
  }

  render() {

    if (this.state.setting) {
      return (
        <div className="user-page">
          <div className="user-setting">
            <div className="user-setting-icon">
              <SettingsIcon onClick={this.toggleSetting}/>
            </div>
          </div>
          <div className="user-page-main">
            <div className="user-name">
              <UserName value={this.state.name} onChange={this.changeName} readonly={!this.state.setting}/>
            </div>

            <div className="user-email">
              <UserEmail value={this.state.email} onChange={this.changeEmail} readonly={!this.state.setting}/>
            </div>

            {/* <div className="user-password">
              <UserPassword value={this.state.password} onChange={this.changePass} readonly={!this.state.setting}/>
            </div> */}

            <div className="user-page-setting-buttons">
              <div className="user-page-cancel-button">
                <Button variant="contained" onClick={this.cancel}>
                  Cancel
                </Button>
              </div>

              <div className="user-page-update-button">
                <Button variant="contained" onClick={this.update}>
                  Update
                </Button>
              </div>
            </div>
          </div>
        </div>
      )
    }

    else {
      return(
        <div className="user-page">
          <div className="user-setting">
            <div className="user-setting-icon">
              <SettingsIcon onClick={this.toggleSetting}/>
            </div>
          </div>
          <div className="user-page-main">
            <div className="user-name">
              <UserName value={this.state.name} onChange={this.changeName} readonly={!this.state.setting}/>
            </div>

            <div className="user-email">
              <UserEmail value={this.state.email} onChange={this.changeEmail} readonly={!this.state.setting}/>
            </div>

            {/* <div className="user-password">
              <UserPassword value={this.state.password} onChange={this.changePass} readonly={!this.state.setting}/>
            </div> */}

            <div className="user-history">
              <div className="user-history-title">History</div>
              <UserHistroy history_articles={this.state.history}/> 
            </div>
          </div>
        </div>
      )
    }
  }
}

export default UserPage