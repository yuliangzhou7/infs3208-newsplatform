import React,  { useState, useEffect } from 'react';
// import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import axios from 'axios';

// Import Components
import Home from "./pages/Home"
import Article from "./pages/Article"
import Dashboard from "./pages/Dashboard"
import User from "./pages/User"
import Authors from "./pages/Authors"
import Author from "./pages/Author"
import About from "./pages/About"
import Header from "./components/common/header"
import Login from "./pages/Login"
import Signon from "./pages/Signon"


function App() {

  // const getFlaskHello = () => {
  //   axios.get('/api/').then(response => {
  //     console.log(response);
  //   });
  // };

  const [user_id, setUser] = useState(null)
  const getUser = () => {

    // Check User Login status
    const user_id = null
    setUser(user_id)
  }

  useEffect(() => getUser(), [])

  return (
    <div>
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Button onClick={getFlaskHello}>Get Flask Hello</Button>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}

      {/* Routing */}
      <Router>
        {/* Header Component */}
        <Header user_id={user_id}/>

        <Switch>
          {/* Article */}
          <Route path="/article/edit/:id" component={Article} />
          <Route path="/article/:id" component={Article} />

          {/* Dashboard */}
          <Route path="/dashboard/:id" component={Dashboard} />
          <Route path="/dashboard" component={Dashboard} />

          {/* User */}
          <Route path="/user/:id" component={User} />

          {/* Authors */}
          <Route path="/authors" component={Authors} />

          {/* Author */}
          <Route path="/author/:id" component={Author} />

          {/* About */}
          <Route path="/about" component={About} />

          {/* Login */}
          <Route path="/login" component={Login} />

          {/* Sign in */}
          <Route path="/signon" component={Signon} />

          {/* Home Page */}
          <Route path="/" component={Home} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
