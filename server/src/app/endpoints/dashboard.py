import uuid
from flask_restful import Resource
from webargs import fields, validate
from webargs.flaskparser import use_kwargs

from ..main import log
from ..db.cassandra import clusterConnect

class PersonalDashboard(Resource):
    args = {
        'id': fields.Str(required=True, location='view_args'),
        'type': fields.Str(required=True, location='querystring'),
    }

    @use_kwargs(args)
    def get(self, id, type):
        session = clusterConnect()
        try:
            # Fetch user's history
            rows = session.execute("""
                SELECT * FROM userhistory WHERE user_id = %s
                """, (uuid.UUID(id),))

            if type == 'tags':
                # Fetch each article's tags from user's history list
                tagsDict = dict()
                for row in rows:
                    result = session.execute("""SELECT * FROM articles WHERE id = %s""",
                        (row.article_id,))
                    article = result.one()

                    tags = article.tags
                    for tag in tags:
                        if tag in tagsDict:
                            tagsDict[tag] = tagsDict[tag] + 1
                        else:
                            tagsDict[tag] = 1

                # Transform into expected output for frontend pie chart
                tagList = []
                for key in tagsDict:
                    tagList.append(dict(label=key, angle=tagsDict[key]))
                myMostViewedTags = sorted(tagList, key=lambda k: k['angle'], reverse=True)[:10]
                totalCount = sum(val['angle'] for val in myMostViewedTags)
                for x in myMostViewedTags:
                    x['angle'] = x['angle']/totalCount*360

                return { 'tag_data': myMostViewedTags }, 200
            elif type == 'authors':
                # Fetch each article from user's history list
                myMostReadAuthors = dict()
                for row in rows:
                    result = session.execute("""SELECT * FROM articles WHERE id = %s""",
                        (row.article_id,))
                    article = result.one()

                    # Count unique authors read by user
                    if str(article.author_id) in myMostReadAuthors:
                        value = myMostReadAuthors[str(article.author_id)]
                        myMostReadAuthors[str(article.author_id)] = dict(
                            x=article.author_name, y=value['y'] + 1)
                    else:
                        myMostReadAuthors[str(article.author_id)] = dict(x=article.author_name, y=1)

                # Transform into output for frontend barchart format
                authorsList = []
                for key, value in myMostReadAuthors.items():
                    authorsList.append(dict(id=key, data=value))

                return { 'author_data': authorsList[:10] }, 200
            else:
                return { 'error': 'Undentified type.'}, 400
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to get personal dashboard.' }, 500

class GlobalDashboard(Resource):
    args = {
        'type': fields.Str(required=True, location='querystring'),
    }

    @use_kwargs(args)
    def get(self, type):
        session = clusterConnect()
        try:
            if type == 'tags':
                # Get all tags
                rows = session.execute("""SELECT * FROM tags""")
                tagList = []
                for row in rows:
                    tagList.append(dict(name=row.name, appearance_count=row.appearance_count))

                # Sort by view_count and get top 10
                orderedList = sorted(tagList, key=lambda k: k['appearance_count'], reverse=True)[:10]
                totalCount = sum(val['appearance_count'] for val in orderedList)

                # Convert view_count into angle
                globalMostPopularTags = []
                for x in orderedList:
                    globalMostPopularTags.append(dict(label=x["name"], 
                        angle=x["appearance_count"]/totalCount*360))

                return { 'tag_data': globalMostPopularTags }, 200
            elif type == 'authors':
                # Get all author of all articles and count with hashmap
                rows = session.execute("""SELECT * FROM articles""")
                authorDict = dict()
                for row in rows:
                    if str(row.author_id) in authorDict:
                        authorDict[str(row.author_id)]["count"] = authorDict[str(row.author_id)]["count"] + 1
                    else:
                        authorDict[str(row.author_id)] = dict(name=row.author_name, count=1)

                # Convert into expected format for bar chart
                mostActiveAuthorsList = []
                for x in authorDict:
                    mostActiveAuthorsList.append(dict(id=x, 
                        data=dict(
                            y=authorDict[x]["count"],
                            x=authorDict[x]["name"],)
                        ))

                return { 'author_data': mostActiveAuthorsList }, 200
            else:
                return { 'error': 'Undentified type.'}, 400
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to get personal dashboard.' }, 500
