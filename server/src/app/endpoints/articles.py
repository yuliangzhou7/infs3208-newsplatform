import uuid
import datetime
from flask_restful import Resource
from webargs import fields, validate
from webargs.flaskparser import use_kwargs

from ..main import log
from ..db.cassandra import clusterConnect

class Articles(Resource):
    args = {
        'author_id': fields.Str(required=True),
        'author_name': fields.Str(required=True),
        'title': fields.Str(required=True),
        'body': fields.Str(required=True),
        'tags': fields.DelimitedList(fields.Str(), required=True)
    }

    def get(self):
        session = clusterConnect()
        try:
            rows = session.execute("""SELECT * from articles""")
            articleList = []
            for row in rows:
                log.info(row)
                articleList.append(dict(
                    id=str(row.id), 
                    author_name=row.author_name, 
                    title=row.title, 
                    tags=row.tags))
                
            return { "articles": articleList }, 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to get articles.' }, 500

    @use_kwargs(args)
    def post(self, author_id, author_name, title, body, tags):
        log.info(tags)
        session = clusterConnect()
        try:
            # TODO: check if author_id exists, if not return 400 error
            # Update tags counter or insert if doesn't exist
            for tag in tags:
                log.warning(tag)
                session.execute("""UPDATE tags SET 
                    appearance_count = appearance_count + 1 WHERE name = %s""", 
                    (tag,))

            # Create new article entry
            article_id = uuid.uuid1()
            session.execute("""INSERT INTO articles (
                id, author_id, author_name, title, body, tags, 
                created_at, last_edited_at, view_count, rating_sum, rating_count)
                VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", 
                (article_id, uuid.UUID(author_id), author_name, title, body, tags, 
                datetime.datetime.now(), datetime.datetime.now(), 0, 0, 0))

            # Retrieve new entry and return results as confirmation
            result = session.execute("""SELECT * FROM articles WHERE id = %s""", (article_id,))
            article = result.one()
            log.info(article)
            return { "article_id": str(article.id),
                "author_id": str(article.author_id),
                "author_name": article.author_name,
                "title": article.title,
                "body": article.body,
                "tags": article.tags,
                "created_at": article.created_at.strftime("%d/%m/%Y, %H:%M"),
                "last_edited_at": article.last_edited_at.strftime("%d/%m/%Y, %H:%M"),
                "average_rating": article.rating_sum/5,
                "view_count": article.view_count }, 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to create article.' }, 500

class ArticlePage(Resource):
    args = {
        'id': fields.Str(required=True, location='view_args'),
        'user_id': fields.Str(required=False),
    }

    @use_kwargs(args)
    def get(self, id, **kwargs):
        session = clusterConnect()
        try:
            # Fetch article contents
            result = session.execute("""SELECT * FROM articles WHERE id = %s""", (uuid.UUID(id),))
            article = result.one()
            log.info(article)

            # Update view count
            session.execute("""UPDATE articles
                SET view_count = %s
                WHERE id = %s AND author_id = %s""", 
                (article.view_count + 1, uuid.UUID(id), article.author_id))

            # Add article to userhistory
            if "user_id" in kwargs:
                session.execute("""INSERT INTO userhistory (
                    user_id, article_id, date_accessed)
                    VALUES(%s, %s, %s)""", (uuid.UUID(kwargs["user_id"]), uuid.UUID(id), datetime.datetime.now()))
                # result = session.execute("""SELECT * FROM userhistory""")
                # log.info(result.one())

            # Calculate average rating score
            if article.rating_count > 0:
                rating = article.rating_sum/article.rating_count
            else:
                rating = 0

            return { "article_id": str(article.id),
                "author_id": str(article.author_id),
                "author_name": article.author_name,
                "title": article.title,
                "body": article.body,
                "tags": article.tags,
                "created_at": article.created_at.strftime("%d/%m/%Y, %H:%M"),
                "last_edited_at": article.last_edited_at.strftime("%d/%m/%Y, %H:%M"),
                "average_rating": rating,
                "view_count": article.view_count + 1 }, 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to get user.' }, 500

    @use_kwargs(args)
    def put(self, id):
        session = clusterConnect()
        try:
            # TODO: to be implemented in future if needed
            return {  }, 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to update user.' }, 500

class RateArticle(Resource):
    args = {
        'id': fields.Str(required=True, location='view_args'),
        'user_id': fields.Str(required=True),
        'rating': fields.Int(required=True, validate=[validate.Range(min=0, max=5)]),
    }

    @use_kwargs(args)
    def post(self, id, user_id, rating):
        session = clusterConnect()
        try:
            result = session.execute("""SELECT * FROM articles WHERE id = %s""", (uuid.UUID(id),))
            article = result.one()
            log.info(article)
            session.execute("""UPDATE articles SET 
                rating_sum = %s , rating_count = %s 
                WHERE id = %s AND author_id = %s""", 
                (article.rating_sum + rating, article.rating_count + 1, uuid.UUID(id), article.author_id))

            session.execute("""UPDATE ratings SET 
                score = %(score)s,
                rated_at = %(rated_at)s
                WHERE article_id = %(article_id)s AND submitted_by = %(submitted_by)s""",
                { 'article_id': uuid.UUID(id),
                'submitted_by': uuid.UUID(user_id),
                'score': rating,
                'rated_at': datetime.datetime.now() })

            return 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to update rating.' }, 500
