import uuid
import json
from flask_restful import Resource
from webargs import fields, validate
from webargs.flaskparser import use_kwargs
from werkzeug import check_password_hash, generate_password_hash

from ..main import log
from ..db.cassandra import clusterConnect

class Users(Resource):
    args = {
        'id': fields.Str(required=True, location='view_args'),
        'name': fields.Str(required=False),
        'old_password': fields.Str(required=False),
        'new_password': fields.Str(required=False)
    }

    @use_kwargs(args)
    def get(self, id, **kwargs):
        session = clusterConnect()
        try:
            # Fetch user by id
            result = session.execute("""SELECT * from users
                WHERE id = %s""", (uuid.UUID(id),))
            user = result.one()

            log.info(user)
            if user == None:
                return { 'error': 'User ' + id + ' does not exist'}, 404

            return { 'id': id, 'email': user.email, 'name': user.name }
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to get user.' }, 500

    @use_kwargs(args)
    def put(self, id, **kwargs):
        session = clusterConnect()
        try:
            # Fetch user by id
            userResult = session.execute("""SELECT * from users
                WHERE id = %s""", (uuid.UUID(id),))
            user = userResult.one()
            if user == None:
                return { 'error': 'User ' + id + ' does not exist'}, 404
            email = user.email
            userCred = session.execute("""SELECT * from usercredentials
                WHERE email = %s""", (email,))
            
            # Fetch old values
            oldName = user.name
            oldhashedPassword = userCred.one().password

            # Update name in users table
            if "name" in kwargs:
                session.execute("""
                    INSERT INTO users (id, email, name)
                    VALUES(%s, %s, %s)
                    """, (uuid.UUID(id), email, kwargs["name"]))

            # Update password in usercredentials table
            if "new_password" in kwargs:
                if "old_password" in kwargs:
                    if check_password_hash(oldhashedPassword, kwargs["old_password"]):
                        session.execute("""
                            INSERT INTO usercredentials (email, password, id)
                            VALUES(%s, %s, %s)
                            """, (email, generate_password_hash(kwargs["new_password"]), uuid.UUID(id)))
                    else:
                        return { "old_password": "Incorrect password entered. Cannot update to new password." }
                else:
                    return { "error": "Previous password not provided. Unable to update password" }, 400
            
            # Fetch updated user by id
            result = session.execute("""SELECT * from users
                WHERE id = %s""", (uuid.UUID(id),))
            user = result.one()
            return { 'id': id, 'email': user.email, 'name': user.name }, 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to update user.' }, 500

class UserHistory(Resource):
    args = {
        'id': fields.Str(required=True, location='view_args'),
    }

    @use_kwargs(args)
    def get(self, id):
        session = clusterConnect()
        try:
            rows = session.execute("""
                SELECT * FROM userhistory WHERE user_id = %s
                """, (uuid.UUID(id),))

            # Fetch articles from user's history list
            articleList = []
            for row in rows:
                log.info(row)
                result = session.execute("""SELECT * FROM articles WHERE id = %s""", 
                    (row.article_id,))
                article = result.one()
                log.info(article)
                articleList.append(dict(
                    date_accessed=row.date_accessed.strftime("%d/%m/%Y, %H:%M"),
                    article_id=str(article.id),
                    author_name=article.author_name, 
                    title=article.title, 
                    tags=article.tags))

            return { 'articles': articleList }, 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to get user history.' }, 500

class Authors(Resource):
    args = {
        'id': fields.Str(required=False, locations=("querystring",)),
    }

    @use_kwargs(args)
    def get(self, **kwargs):
        session = clusterConnect()
        # Get specific author and author's articles
        if "id" in kwargs:
            # Fetch user details
            user_id = kwargs["id"]
            result = session.execute("""SELECT * FROM users 
                WHERE id = %s""", (uuid.UUID(user_id),))
            user = result.one()
            log.info(user)

            # Fetch all articles written by user
            rows = session.execute("""SELECT * from articles 
                WHERE author_id = %s ALLOW FILTERING""", (user.id,))
            articleList = []
            for row in rows:
                log.info(row)
                articleList.append(dict(
                    id=str(row.id), title=row.title, tags=row.tags))

            return { 'id': str(user.id), "name": user.name, "articles": articleList }
        else:
            # Get all authors as a list
            try:
                rows = session.execute("""SELECT id, name FROM users""")
                authorsList = []
                for row in rows:
                    log.info(row)
                    authorsList.append(dict(id=str(row.id), name=row.name))
                log.info(authorsList)
                return { 'authors': authorsList }
            except Exception as e:
                log.error(e)
                return { 'error': 'Internal Error. Unable to get authors.' }, 500
