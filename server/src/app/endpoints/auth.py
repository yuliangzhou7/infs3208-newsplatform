import uuid
from flask_restful import Resource
from webargs import fields, validate
from webargs.flaskparser import use_kwargs
from werkzeug import check_password_hash, generate_password_hash

from ..main import log
from ..db.cassandra import clusterConnect

class Login(Resource):
    args = {
        'email': fields.Str(required=True),
        'password': fields.Str(required=True),
    }

    @use_kwargs(args, locations=('json',))
    def post(self, email, password):
        session = clusterConnect()
        try:
            # Fetch user by email
            result = session.execute("""SELECT * from usercredentials 
                WHERE email = %s""", (email,))
            user = result.one()

            # Email doesn't exist
            if user == None:
                return { "error":  "Unable to login with provided credentials." }, 400

            if check_password_hash(user.password, password):
                return { 'token': str(user.id)}
            else:
                return { "error":  "Unable to login with provided credentials." }, 400
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to login.' }, 500

class Signup(Resource):
    args = {
        'email': fields.Str(required=True),
        'name': fields.Str(required=True),
        'password': fields.Str(required=True),
    }

    @use_kwargs(args, locations=('json',))
    def post(self, email, name, password):
        session = clusterConnect()
        try:
            # Verify email is not taken
            result = session.execute("""SELECT count(*) from usercredentials 
                WHERE email = %s""", (email,))
            if result.one().count != 0:
                return { 'error': email + ' already exists'}, 400

            # Insert new user
            log.info("Adding new user: " + email)
            user_id = uuid.uuid1()
            session.execute("""
                INSERT INTO users (id, email, name)
                VALUES(%s, %s, %s)
                """, (user_id, email, name))
            session.execute("""
                INSERT INTO usercredentials (email, password, id)
                VALUES(%s, %s, %s)
                """, (email, generate_password_hash(password), user_id))
            result = session.execute("""SELECT id FROM users WHERE id = %s""", (user_id,))

            # Return id as token. Temporary, will be updated once JWTs implemented
            log.info(result.one())
            log.info(str(result.one().id))
            return { 'token': str(user_id) }
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to add new user.' }, 500
