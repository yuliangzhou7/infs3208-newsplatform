from flask_restful import Resource
import os

from ..main import log

class HealthCheck(Resource):
    def get(self):
        log.info(os.environ.get('FLASK_ENV'))
        return {'status': 'OK'}, 200
