from flask_restful import Resource

from ..main import log
from ..db.cassandra import clusterConnect

class PopularArticles(Resource):
    def get(self):
        session = clusterConnect()
        try:
            # Get all articles
            rows = session.execute("""SELECT * FROM articles""")
            articleList = []
            for row in rows:
                articleList.append(dict(id=str(row.id), author_name=row.author_name, 
                    title=row.title, tags=row.tags, view_count=row.view_count))

            # Sort by view_count
            orderedList = sorted(articleList, key=lambda k: k['view_count'], reverse=True)

            return { 'articles': orderedList[:5] }, 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to get most popular articles.' }, 500

class PopularTags(Resource):
    def get(self):
        session = clusterConnect()
        try:
            # Get all tags
            rows = session.execute("""SELECT * FROM tags""")
            tagList = []
            for row in rows:
                tagList.append(dict(name=row.name, appearance_count=row.appearance_count))

            # Sort by view_count
            orderedList = sorted(tagList, key=lambda k: k['appearance_count'], reverse=True)

            return { 'tags': orderedList[:5] }, 200
        except Exception as e:
            log.error(e)
            return { 'error': 'Internal Error. Unable to get most popular tags.' }, 500
