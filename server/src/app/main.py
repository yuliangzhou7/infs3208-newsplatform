from flask import Flask
from flask_restful import Resource, Api
from cassandra.cluster import Cluster
from webargs.flaskparser import parser, abort
import logging

# Setup logging
log = logging.getLogger()
log.setLevel('INFO')
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s"))
log.addHandler(handler)

# Create Flask object
app = Flask(__name__)
api = Api(app)

# This error handler is necessary for usage with Flask-RESTful
@parser.error_handler
def handle_request_parsing_error(err, req, schema, error_status_code, error_headers):
    """webargs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    abort(error_status_code, errors=err.messages)

# Import sub-modules after initialising flask to avoid circiular imports
from app.db.seed import Keyspace
from app.endpoints.healthcheck import HealthCheck
from app.endpoints.auth import Login, Signup
from app.endpoints.accounts import Users, UserHistory, Authors
from app.endpoints.articles import Articles, ArticlePage, RateArticle
from app.endpoints.popular import PopularArticles, PopularTags
from app.endpoints.dashboard import PersonalDashboard, GlobalDashboard

# Attach classful object to URL endpoint
api.add_resource(Keyspace, '/keyspace')
api.add_resource(HealthCheck, '/')
api.add_resource(Login, '/login')
api.add_resource(Signup, '/signup')
api.add_resource(Users, '/users/<id>')
api.add_resource(UserHistory, '/users/<id>/history')
api.add_resource(Authors, '/authors', '/authors/<id>')
api.add_resource(Articles, '/articles')
api.add_resource(ArticlePage, '/articles/<id>')
api.add_resource(RateArticle, '/articles/<id>/rate')
api.add_resource(PopularArticles, '/popular/articles')
api.add_resource(PopularTags, '/popular/tags')
api.add_resource(GlobalDashboard, '/dashboard')
api.add_resource(PersonalDashboard, '/dashboard/<id>')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
