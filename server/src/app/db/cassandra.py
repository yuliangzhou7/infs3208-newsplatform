from cassandra.cluster import Cluster
from cassandra.policies import DCAwareRoundRobinPolicy

def clusterConnect(keyspace = 'newsplatformkeyspace'):
    cluster = Cluster(contact_points=['cassandra0'], port=9042, load_balancing_policy=DCAwareRoundRobinPolicy())
    session = cluster.connect(keyspace)
    return session
