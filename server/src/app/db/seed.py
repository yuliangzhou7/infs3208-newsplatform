import uuid
from flask_restful import Resource
from cassandra.cluster import Cluster
from webargs import fields, validate
from webargs.flaskparser import use_kwargs
from werkzeug import check_password_hash, generate_password_hash

from ..main import log
from ..db.cassandra import clusterConnect

class Keyspace(Resource):
    args = {
        'password': fields.Str(required=True),
    }

    # POST: create keyspace and tables
    @use_kwargs(args)
    def post(self, password):
        if (password != 'create123'):
            return { 'auth': 'failed', 'reason': 'Incorrect password: ' + password }

        # Connect without keyspace since it has yet to be created
        cluster = Cluster(contact_points=['cassandra0'], port=9042)
        session = cluster.connect()

        KEYSPACE = "newsplatformkeyspace"
        try:
            log.info("Creating keyspace...")
            session.execute("""
                CREATE KEYSPACE %s
                WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
                """ % KEYSPACE)
            log.info("Setting keyspace...")
            session.set_keyspace(KEYSPACE)
            log.info("Creating users table...")
            session.execute("""
                CREATE TABLE users (
                    id uuid,
                    email text,
                    name text,
                    PRIMARY KEY (id, email)
                )""")
            log.info("Creating usercredentials table...")
            session.execute("""
                CREATE TABLE usercredentials (
                    email text,
                    password text,
                    id uuid,
                    PRIMARY KEY (email)
                )""")
            log.info("Creating articles table...")
            session.execute("""
                CREATE TABLE articles (
                    id uuid,
                    author_id uuid,
                    author_name text,
                    title text,
                    body text,
                    tags list<text>,
                    created_at timestamp,
                    last_edited_at timestamp,
                    view_count int,
                    rating_sum int,
                    rating_count int,
                    PRIMARY KEY (id, author_id)
                )""")
            log.info("Creating tags table...")
            session.execute("""
                CREATE TABLE tags (
                    name text,
                    appearance_count counter,
                    PRIMARY KEY (name)
                )""")
            log.info("Creating ratings table...")
            session.execute("""
                CREATE TABLE ratings (
                    article_id uuid,
                    submitted_by uuid,
                    score int,
                    rated_at timestamp,
                    PRIMARY KEY (article_id, submitted_by)
                )""")
            log.info("Creating userhistory table...")
            session.execute("""
                CREATE TABLE userhistory (
                    user_id uuid,
                    article_id uuid,
                    date_accessed timestamp,
                    PRIMARY KEY (user_id, article_id)
                )""")
            return { 'operation': 'success' }
        except Exception as e:
            log.error("Unable to create keyspace")
            log.error(e)
            return { 'operation': 'failed', 'reason': KEYSPACE+' keyspace already exists' }

    # PUT: seed keyspace/tables with placeholder values
    @use_kwargs(args)
    def put(self, password):
        if (password != 'seed123'):
            return { 'auth': 'failed', 'reason': 'Incorrect password' }
        session = clusterConnect()
        try:
            result = session.execute("""SELECT count(*) from usercredentials 
                WHERE email = %s""", ("tester@example.com",))
            if result.one().count != 0:
                return { 'operation': 'failed', 'reason': 'tester@example.com already exists'}

            log.info("Adding a single entry to users...")
            user_id = uuid.uuid1()
            session.execute("""
                INSERT INTO users (id, email, name)
                VALUES(%s, %s, %s)
                """, (user_id, "tester@example.com", "tester"))
            log.info("Adding a single entry to usercredentials...")
            session.execute("""
                INSERT INTO usercredentials (email, password, id)
                VALUES(%s, %s, %s)
                """, ("tester@example.com", generate_password_hash('password123'), user_id))
            return { 'operation': 'success', 'id': str(user_id) }
        except Exception as e:
            log.error(e)
            return { 'operation': 'failed' }

    # Dev purposed only
    @use_kwargs(args)
    def delete(self, password):
        if (password != 'delete123'):
            return { 'auth': 'failed', 'reason': 'Incorrect password' }
        session = clusterConnect()
        KEYSPACE = "newsplatformkeyspace"
        try:
            log.info("Deleting keyspace...")
            session.execute("""DROP KEYSPACE %s """ % KEYSPACE)
            return { 'operation': 'success' }
        except Exception as e:
            log.error(e)
            return { 'operation': 'failed' }
