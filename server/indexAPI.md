# Flask RESTAPIDocs

Documentation for all `/api/` endpoints available through the Flask server.

## Open Endpoints

Open endpoints require no Authentication.

### General APIs:
* [Signup](api-reference/signup.md) : `POST /api/signup/`
* [Login](api-reference/login.md) : `POST /api/login/`
* [Get 5 Most Popular Articles](api-reference/popularArticles.md) : `GET /api/popular/articles`
* [Get 5 Most Popular Tags](api-reference/popularTags.md) : `GET /api/popular/tags`

### Author related:
* [Get Author's details](api-reference/authors/id/get.md) : `GET /api/authors/<id>`
* [Get list of all authors](api-reference/authors/get.md) : `GET /api/authors`

### Article related:
* [Get Articles List](api-reference/articles/get.md) : `GET /api/articles/`
* [Get specific article](api-reference/articles/id/get.md) : `GET /api/articles/<id>`

## Endpoints that require Authentication

Closed endpoints require a valid Token to be included in the header of the
request. A Token can be acquired from the Login view above.

### Current User related:

Each endpoint manipulates or displays information related to the User whose
Token is provided with the request:

* [Get my account details](api-reference/users/id/get.md) : `GET /api/user/<id>`
* [Update my account details](api-reference/users/id/put.md) : `PUT /api/user/<id>`
* [Get my history](api-reference/users/id/history/get.md) : `GET /api/users/<id>/history`

### Article related:
* [Create Article](api-reference/articles/post.md) : `POST /api/articles/`
* [Update Article](api-reference/articles/id/put.md) : `PUT /api/articles/<id>`
* [Submit rating for an article](api-reference/articles/id/rate/post.md) : `POST /api/rate/<article_id>`

### Dashboard related:
* [Get Dashboard Global Data](api-reference/dashboard/get.md) : `GET /api/dashboard` 
* [Get Dashboard Personal Data](api-reference/dashboard/id/get.md) : `GET /api/dashboard/<user_id>` 
