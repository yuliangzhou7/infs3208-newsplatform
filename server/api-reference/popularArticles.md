
# Popular Articles

Retrieve preview of top 5 most popular articles.

**URL** : `/api/popular/articles/`

**Method** : `GET`

**Auth required** : NO

## Success Response

**Code** : `200 OK`

**Content Example** :

```json
{
  "articles": [
    {
      "id": "article-id-001",
      "authorName": "autthor-name",
      "title": "Sample Article",
      "tags": ["sample-1", "sample-2"],
      "view_count": 5
    },
    {
      "id": "article-id-002",
      "authorName": "autthor-name",
      "title": "Sample Article1",
      "tags": ["sample-1", "sample-2"],
      "view_count": 3
    },
  ]
}
```
