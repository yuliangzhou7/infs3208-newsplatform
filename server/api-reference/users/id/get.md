# Show Current User

Get the details of the currently Authenticated User along with basic
subscription information.

**URL** : `/api/users/<id>`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content examples**

For a User with ID 1234 is also added to post data as well as url

```json
{
    "id": 1234,
    "name": "Joe Bloggs",
    "email": "joe25@example.com"
}
```
