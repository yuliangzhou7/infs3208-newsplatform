# Show Current User's History

Get list of articles user has read.

**URL** : `/api/users/<id>/history`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content examples**

```json
{
  "articles": [
    {
      "date_accessed": "date",
      "article_id": "article-id-001",
      "author_name": "author-name",
      "title": "Sample Article",
      "tags": ["sample-1", "sample-2"]
    },
    {
      "date_accessed": "date",
      "article_id": "article-id-002",
      "author_name": "author-name",
      "title": "Sample Article1",
      "tags": ["sample-1", "sample-2"]
    },
  ]
}
```
