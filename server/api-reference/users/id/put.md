# Update Current User

Allow the Authenticated User to update their details.

**URL** : `/api/users/<id>`

**Method** : `PUT`

**Auth required** : YES

**Permissions required** : None

**Data constraints**

```json
{
    "name": "[1 to 30 chars]",
    "old_password": "password123",
    "new_password": "newpassword123"
}
```

Note that `id` and `email` are currently read only fields.
`old_password` is required if `new_password` is set

**Data examples**

Partial data is allowed.

```json
{
    "name": "John"
}
```

## Success Responses

**Condition** : Data provided is valid and User is Authenticated.

**Code** : `200 OK`

**Content example** : Response will reflect back the updated information.
Will never display password back.

```json
{
    "id": 1234,
    "email": "joe25@example.com",
    "name": "Joe"
}
```

## Error Response

**Condition** : If provided data is invalid, e.g. a name field is too long

**Code** : `400 BAD REQUEST`

**Content example** :

```json
{
    "name": "Please provide maximum 30 character or empty string"
}
```

```json
{
    "old_password": "Incorrect password entered. Cannot update to new password."
}
```

## Notes

* Endpoint will ignore irrelevant and read-only data such as parameters that
  don't exist, or fields that are not editable like `id` or `email`.
