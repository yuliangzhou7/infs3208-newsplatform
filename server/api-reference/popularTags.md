
# Popular Articles

Retrieve top 5 most popular tags.

**URL** : `/api/popular/tags/`

**Method** : `GET`

**Auth required** : NO

## Success Response

**Code** : `200 OK`

**Content Example** :

```json
{
  "tags": ["sample-1", "sample-2", "sample-3", "sample-4", "sample-5"],
}
```
