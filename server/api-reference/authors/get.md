
# Get Authors List

Used to get author details.

**URL** : `/api/authors`

**Method** : `GET`

**Auth required** : NO

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "authors": [
      {
        "id": "author-id-001",
        "name": "author smaple",
      },
      {
        "id": "author-id-002",
        "name": "author smaple1",
      },
    ]
}
```
