
# Get Author Details

Used to get author details.

**URL** : `/api/authors/<id>`

**Method** : `GET`

**Auth required** : NO

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "id": "author-id-001",
    "name": "author smaple",
    "articles": [
      {
        "id": "article-id-1",
        "title": "Sample Article",
        "tags": ["sample-1", "sample-2"]
      }
    ]
}
```
