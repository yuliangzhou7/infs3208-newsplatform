
# Create Article

Used to create an article.

**URL** : `/api/articles/`

**Method** : `POST`

**Auth required** : NO

**Request Body**
```json
{
  "author_id": "current_user_id",
  "author_name": "current_user_name",
  "title": "Sample Article",
  "body": "Sample Body",
  "tags": ["sample-1", "sample-2"]
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "article_id": "assigned uid of article",
  "author_id": "current_user_id",
  "author_name": "current_user_name",
  "title": "Sample Article",
  "body": "Sample Body",
  "tags": ["sample-1", "sample-2"],
  "created_at": "timestamp",
  "last_edited_at": "timestamp",
  "average_rating": "rating_score",
  "view_count": 4,
}
```
