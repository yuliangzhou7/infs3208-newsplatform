
# Submit Rating

Used to submit rating. Overwrites previous response if already exists.

**URL** : `/api/articles/<article_id>/rate`

**Method** : `POST`

**Auth required** : NO

**Request Body**
```json
{
  "user_id": "current_user_id",
  "rating": 3
}
```

## Success Response

**Code** : `200 OK`
