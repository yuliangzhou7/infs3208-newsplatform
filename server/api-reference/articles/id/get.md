
# Get Article Details

Used to get article details.

**URL** : `/api/articles/<id>`

**Method** : `GET`

**Auth required** : NO

**Query String**

Optional user_id for tracking user browsing history
```
/api/articles/<id>?user_id=<id>
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "article_id": "assigned uid of article",
  "author_id": "user_id_of_author",
  "title": "Sample Article",
  "body": "Sample Body",
  "tags": ["sample-1", "sample-2"],
  "created_at": "timestamp",
  "last_edited_at": "timestamp",
  "average_rating": "rating_score",
  "view_count": 4,
}
```
