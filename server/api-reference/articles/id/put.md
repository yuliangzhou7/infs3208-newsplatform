
# Update Article Details

Used to update article details.

**URL** : `/api/articles/<id>`

**Method** : `PUT`

**Auth required** : NO

**Request Body**
```json
{
  "id": "article-id-1",
  "title": "Sample Article",
  "sub_title": "Sample Sub Title",
  "tags": ["sample-1", "sample-2"]
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "id": "article-id-1",
  "title": "Sample Article",
  "sub_title": "Sample Sub Title",
  "tags": ["sample-1", "sample-2"]
}
```
