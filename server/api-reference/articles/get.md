
# Get Articles Details

Used to get articles details.

**URL** : `/api/articles/`

**Method** : `GET`

**Auth required** : NO

**Optional URL Parameter**
In development
```
?tags="sample-tag-1,sample-tag-2"
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "articles": [
    {
      "id": "article-id-001",
      "authorName": "autthor-name",
      "title": "Sample Article",
      "tags": ["sample-1", "sample-2"]
    },
    {
      "id": "article-id-002",
      "authorName": "autthor-name",
      "title": "Sample Article1",
      "tags": ["sample-1", "sample-2"]
    },
  ]
}
```
