
# Signup

**URL** : `/api/signup/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "email": "[valid email address]",
    "name": "[username or fullname]",
    "password": "[password in plain text]"
}
```

**Data example**

```json
{
    "email": "iloveauth@example.com",
    "name": "bobby123",
    "password": "abcd1234"
}
```

## Success Response

**Code** : `200 OK`

For now the token returned is the user's UUID

**Content example**

```json
{
    "token": "93144b288eb1fdccbe46d6fc0f241a51766ecd3d"
}
```

## Error Response

**Condition** : If 'username' already exists.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "error": [
        "Email already exists"
    ]
}
```