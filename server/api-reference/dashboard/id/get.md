
# Personal Dashboard Data

Get current user's personal viewing data.

**URL** : `/api/dashboard/<id>`

**Method** : `GET`

**Auth required** : YES

**Query String**

Required `type`
```
/api/dashboard/<id>?type=<tags || authors>
```

## Success Response

**Code** : `200 OK`

**Content example**

Type Authors (BarChart): My most viewed authors
```json
{
    "author_data": [
        {
            "id": "author_id",
            "data": {
                "y": "amount",
                "x": "author name"
            }
        },
        {
            "id": "author_id",
            "data": {
                "y": "amount",
                "x": "author name"
            }
        },
    ]
}
```

Type Tags (PieChart): My most viewed tags
```json
{
    "tag_data": [
        {
            "label": "label name",
            "angle": "amount"
        },
        {
            "label": "label name",
            "angle": "amount",
        },
    ]
}
```
