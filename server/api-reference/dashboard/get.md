
# Global Dashboard Data

Get global data related/available to all users.

**URL** : `/api/dashboard`

**Method** : `GET`

**Auth required** : NO

**Query String**

Required `type`
```
/api/dashboard?type=<tags || authors>
```

## Success Response

**Code** : `200 OK`

**Content example**

Type Authors (BarChart): Author with most articles
```json
{
    "something": [
        {
            "id": "author/article id",
            "data": {
                "y": "amount",
                "x": "author name/article title"
            }
        },
        {
            "id": "author/article id",
            "data": {
                "y": "amount",
                "x": "author name/article title"
            }
        },
    ]
}
```

Type Tags (PieChart): Most popular Tags
```json
{
    "something": [
        {
            "label": "label name",
            "angle": "amount"
        },
        {
            "label": "label name",
            "angle": "amount"
        },
    ]
}
```
